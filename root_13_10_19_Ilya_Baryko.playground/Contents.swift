import Foundation

//  task 1

func task1(h: Double, bounce: Double, window: Float) -> Int {
    guard ((h > 0) && (Double(window) < h) && ((bounce > 0) && (bounce < 1))) else {
        return -1
    }
        var newHeight = h * bounce          //  Высота отскока
        var numOfTimes = 1          //  Кол-во раз, которое увидела мать
        while (newHeight > Double(window)) {
            numOfTimes += 2
            newHeight *= bounce
        }
        return numOfTimes
    }

print("task1 = \(task1(h: 10, bounce: 1.5, window: 5))")

//  task2

func multiply(a: Int) -> Int {
    if(a > 0) {
        if(a % 2 == 0) {
            return a * 8
        } else {
            return a * 9
        }
    } else {
        return a
    }
}

print("task2 = \(multiply(a: 3))")

//  task 3

func toInt(a: String) -> Int {
    return Int(a)!
}

func sumOfArray(array: [Any] = []) -> Int {
    var sum = 0
    if(array.count != 0) {
        for item in array {
            switch item {
            case is String:
                sum += toInt(a: item as! String)
            default:
                sum += item as! Int
            }
        }
    return sum
    } else {
        return -1
    }
}

print("task3 = \(sumOfArray(array: [3, 4, "5", 6]))")

//  task 4

func getCentury(year: Int) -> Int {
    if(year >= 0) {
        var century = Int(year / 100)
        if((year % 100) > 0) {
            century += 1
        }
        return century
    } else {
        return 0
    }
}

print("task4 = \(getCentury(year: 0))")

//  task 5

typealias ageAndYears = (Int, Int)
//  Функция, которая возвращает, на какое число изменить возраст и кол-во лет после отнимания промежутка вермени, которое считается за человеческий год
func aged(years: Int, step: Int) -> ageAndYears {
    if((years - step) >= 0) {
        return (1, (years - step))
    } else {
        return (0, 0)
    }
}
//  Функция, которая возвращает кол-во человеческих лет питомца в руках человека
func getOwnYears(line: Int, thirdStep: Int) -> Int {
    var ownTime = 0
    var value = aged(years: line, step: 15)
    ownTime += value.0
    value = aged(years: value.1, step: 9)
    ownTime += value.0
    while(value.1 > 0) {
        value = aged(years: value.1, step: thirdStep)
        ownTime += value.0
    }
    return ownTime
}

func catsAndDogs(catYears: Int, DogYears: Int) -> (Int, Int) {
    let ownCat = getOwnYears(line: catYears, thirdStep: 4)
    let ownDog = getOwnYears(line: DogYears, thirdStep: 5)
    return (ownCat, ownDog)
}

print("task5 = \(catsAndDogs(catYears: 32, DogYears: 33))")

//  task 6

func rowSumOddNumbers(rowNum: Int) -> Int {
    let triangle: [[Int]] = [[0, 0, 1, 0, 0], [0, 3, 0, 5, 0], [0, 7, 9, 11, 0], [0, 13, 15, 17, 19], [21, 23, 25, 27, 29]]
    var sum = 0
    if (rowNum > 0 && rowNum <= triangle.count) {
        for i in 0...(triangle.count - 1) {
                sum += triangle[rowNum - 1][i]
        }
    }
    return sum
}

print("task6 = \(rowSumOddNumbers(rowNum: 3))")

//  task 7

func triangular(count: Int) -> Int {
    if (count > 0) {
        return ((count * (count + 1)) / 2)
    } else {
        return 0
    }
}

print("task7 = \(triangular(count: 3))")

//  task 8
//  Функция возвращает число, возведённое в степень
func toInt(str: String, pos: Int, count: Int) -> Int {
    let b: Int? = Int(String(str[str.index(str.startIndex, offsetBy: pos)]))
    let f = Int(pow(Double(b!), Double(count)))
    return f
}

func armstrongNum(num: Int) -> Bool {
    if(num < 0) {
        return false
    }
    let stringNum = String(num)
    var result = false
    var resultNum = 0
    for i in 0...(stringNum.count - 1) {
        resultNum += toInt(str: stringNum, pos: i, count: stringNum.count)
    }
    if(resultNum == num) {
        result = true
    } else {
        result = false
    }
    return result
}

print("task8 = \(armstrongNum(num: 153))")

//  task 9

func multiplyMatrix(firstArray: [[Int]], secondArray: [[Int]]) -> [[Int]] {
    var resultArray: [[Int]] = []
    if((firstArray.count == firstArray[0].count) && (secondArray.count == secondArray[0].count) && (firstArray.count == secondArray.count)) {
        var row: [Int] = []
        for i in 0...(firstArray.count - 1) {
            for k in 0...(firstArray.count - 1) {
                var sumForNewElem = 0
                for j in 0...(firstArray.count - 1) {
                    sumForNewElem += firstArray[i][j] * secondArray[j][k]
                }
                row.append(sumForNewElem)
            }
            resultArray.append(row)
            row.removeAll()
        }
    } else {
        print("Enter the square matrix")
    }
    return resultArray
}

print("task9 = \(multiplyMatrix(firstArray: [[1, 2], [3, 2]], secondArray: [[3, 2], [1, 1]]))")

//  task 10

func getSortedArray(firstArray: [String], secondArray: [String]) -> [String] {
    var resultArray: [String] = []
    for i in (0...firstArray.count - 1) {
        for j in 0...(secondArray.count - 1) {
            if(secondArray[j].contains(firstArray[i])) {
                if(!resultArray.contains(firstArray[i])) {
                    resultArray.append(firstArray[i])
                }
            }
        }
    }
    resultArray.sort()
    return resultArray
}

print("task10 = \(getSortedArray(firstArray: ["live", "arp", "strong"], secondArray: ["lively", "alive", "alive", "harp", "sharp", "armstrong"]))")
